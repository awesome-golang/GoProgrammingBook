Solution to exercise from **The Go Programming Language**.

### How to run go file
```bash
> export EXPATH=~/local-path/GoProgrammingBook
> go run $EXPATH/ch01-ex01.go
```
