package main

import (
	"fmt"
	"os"
	"strings"
)

// join all element in slices os.Args
func main () {
	fmt.Println(strings.Join(os.Args[0:], " "))
}